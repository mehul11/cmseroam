<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class User extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = [
        'name', 'username', 'password','type','social_type','social_id','active','img_path','remember_token','gender','contact_no','user_id','licensee_id','domain_id'
    ];
    protected $table = 'users';
    protected $primaryKey = 'id';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function customer() {
        return $this->hasOne('App\Customer');
    }
    
    public static function getUserList($sUserType,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10){
        return User::from('users as u')
                    ->where('u.type',$sUserType)
                    ->when($sSearchStr, function($query) use($sSearchStr) {
                        $query->where(function($query1) use($sSearchStr) {
                            $query1->where('u.name','like','%'.$sSearchStr.'%')
                                   ->orWhere('u.username','like','%'.$sSearchStr.'%');
                            });
                        })
                    ->select(
                        'u.id as id',
                        'u.name as name',
                        'u.username as username',
                        'u.active as active',
                        'u.type as type',
                        'u.created_at as created_at',
                        'u.updated_at as updated_at'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    //->toSql();
                    ->paginate($nShowRecord);
    }

    public function user_domains() {
        return $this->hasMany('App\UserDomain','user_id');
    }
    public static function getAgentUserList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10,$licensee_id,$domain_id){
    return User::from('users as u')
            ->where('user_id',Auth::user()->id)
            ->where('licensee_id',$licensee_id)
            ->where('domain_id',$domain_id)
            ->when($sSearchStr, function($query) use($sSearchStr) {
                $query->where(function($query1) use($sSearchStr) {
                    $query1->where('u.name','like','%'.$sSearchStr.'%')
                           ->orWhere('u.username','like','%'.$sSearchStr.'%');
                    });
                })
            ->select(
                'u.id as id',
                'u.name as name',
                'u.username as username',
                'u.active as active',
                'u.type as type',
                'u.created_at as created_at',
                'u.updated_at as updated_at'
                )
            ->orderBy($sOrderField, $sOrderBy)
            ->paginate($nShowRecord);
        
    }
}
