<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licensee extends Model
{
    protected $fillable = [
        'user_id', 'description', 'domain','expiry_date','business_name','first_name',
        'last_name','email','phone_number','street','apartment','city','country_id','state',
        'timezone_id','website','zip_code'
    ];
    protected $table = 'licensees';
    protected $primaryKey = 'id';   

    public function timezone() {
        return $this->belongsTo('App\Timezone');
    }

    public function country() {
    	return $this->belongsTo('App\Country');
    }

    public function domains() {
        return $this->hasMany('App\Domain','licensee_id');
    }
    
    public function domain() {
        return $this->belongsTo('App\DomainFrontend');
    }
}
