<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Libraries\Viator;
use DB;
use File;
use App\ViatorData;
use App\ViatorLocation;
use App\ActivityNew;
use App\ActivityBasePrice;
use App\ActivityMarkup;
use App\ActivityPrice;
use App\ActivityImage;
use App\ViatorCategory;
use App\CategoryDef;
use Illuminate\Support\Facades\Log;

class ViatorApiController extends Controller
{
    public function __construct()
    {
        $this->api = New Viator;
        $this->url  = config('viator_variables.viator_url');
        $this->api_key  = config('viator_variables.api_key');
    }


    public function activities(\Illuminate\Http\Request $request){   
        $data = Input::all();
        $activities = $this->api->activities($data);

        $domain = $request->header('domain');

        $path = '/search/products/';  
        Log::channel('viator_detail')->info('Request : '.$this->url.$path.'?apiKey='.$this->api_key. json_encode($data));

        if(is_object($activities)) {
            $activities = json_decode(json_encode($activities),true);
            $activities = $activities['original'];
            if ($activities['data'] == null) {
                $activities['data'] = [];
            }
        }


        if ($domain && !empty($activities['data'])) {
            $commissions = getDomainData($domain);
            if ($commissions['pricing'][0]['status'] == 0) {
                $activities = updatePricing($activities,'activity','viator',$commissions['eroam'],$commissions['pricing']);
            }
        }

        Log::channel('viator_detail')->info('Response : '.json_encode($activities));
        Log::channel('viator_detail')->info('--------------------------------------------');
        return $activities;
    }

    public function bookActivities()
    {   
        $data = Input::all();
        $data = json_encode($data);
        //$data = '{"demo":"false","currencyCode":"AUD","partnerDetail":{"distributorRef":"The eRoam Reference for productCode : 5297EC72"},"booker":{"firstname":"jjjj","surname":"salon","title":"","email":"688@test.com","homePhone":"1234645"},"items":[{"partnerItemDetail":{"distributorItemRef":"The eRoam Item Reference for productCode : 3337PFWC0"},"hotelId":"null","pickupPoint":"null","travelDate":"2018-08-22","productCode":"3337PFWC","tourGradeCode":"DEFAULT","languageOptionCode":"en\/SERVICE_GUIDE","travellers":[{"bandId":1,"firstname":"jjjj","surname":"salon","leadTraveller":"true"},{"bandId":1,"firstname":"yyyy","surname":"salon"},{"bandId":2,"firstname":"dsad","surname":"eqwe"},{"bandId":2,"firstname":"hjg","surname":"sad"}]},{"partnerItemDetail":{"distributorItemRef":"The eRoam Item Reference for productCode : 5297EC71"},"hotelId":"null","pickupPoint":"null","travelDate":"2018-08-22","productCode":"5297EC7","tourGradeCode":"EC8_FREMANTLE","languageOptionCode":"en\/SERVICE_GUIDE","travellers":[{"bandId":1,"firstname":"jjjj","surname":"salon","leadTraveller":"true"},{"bandId":1,"firstname":"yyyy","surname":"salon"},{"bandId":1,"firstname":"dsad","surname":"eqwe"},{"bandId":2,"firstname":"hjg","surname":"sad"}]},{"partnerItemDetail":{"distributorItemRef":"The eRoam Item Reference for productCode : 5297EC72"},"hotelId":"null","pickupPoint":"null","travelDate":"2018-08-23","productCode":"5297EC7","tourGradeCode":"EC8_FREMANTLE","languageOptionCode":"en\/SERVICE_GUIDE","travellers":[{"bandId":1,"firstname":"jjjj","surname":"salon","leadTraveller":"true"},{"bandId":1,"firstname":"yyyy","surname":"salon"},{"bandId":1,"firstname":"dsad","surname":"eqwe"},{"bandId":2,"firstname":"hjg","surname":"sad"}]}]}';


        $data = str_replace('\/','/', $data);
        $data = str_replace('"null"','null', $data);
        $data = str_replace('"false"','false', $data);
        $data = str_replace('"true"','true', $data);

        $path = '/booking/book';   
        Log::channel('viator_booking')->info('Request : '.$this->url.$path.'?apiKey='.$this->api_key. $data);

        $response = $this->api->bookActivities($data);        

        $return_data['successful'] =true;
        $return_data = array();
        if(!empty($response)){
            if(!empty($response['data'])){
                $return_data['booking_id'] = $response['data']['itineraryId'];
                $return_data['provider_booking_status'] = $response['data']['bookingStatus']['type'];
                $return_data['booking_error_code'] = '';
                $return_data['voucherKey'] = $response['data']['voucherKey'];
                $return_data['voucherURL'] = $response['data']['voucherURL'];
                $return_data['tourGradeCode'] = $response['data']['itemSummaries'][0]['tourGradeCode'];
                $return_data['merchantNetPrice'] = $response['data']['itemSummaries'][0]['merchantNetPrice'];

                $return_data['itineraryId'] = $response['data']['itemSummaries'][0]['itineraryId'];
                $return_data['distributorRef'] = $response['data']['distributorRef'];
                $return_data['itemId'] = $response['data']['itemSummaries'][0]['itemId'];
                $return_data['distributorItemRef'] = $response['data']['itemSummaries'][0]['distributorItemRef'];
                $return_data['successful'] =true;
            }else{
                $return_data['provider_booking_status'] = $response['errorMessage'][0];
                $return_data['booking_error_code'] = $response['errorCodes'][0];
                $return_data['booking_id'] = '';
                $return_data['voucherKey'] = '';
                $return_data['voucherURL'] = '';
                $return_data['successful'] =false;
                $return_data['tourGradeCode'] = '';
                $return_data['merchantNetPrice'] = '';
                $return_data['itineraryId'] = '';
                $return_data['distributorRef'] = '';
                $return_data['itemId'] = '';
                $return_data['distributorItemRef'] = '';
            }
        }

        Log::channel('viator_booking')->info('Response : '.json_encode($response));
        Log::channel('viator_booking')->info('--------------------------------------------');

        
        return $return_data;
        
    }
    public function getTourCode(){
        $data = Input::All(); // get all request data

        $price = str_replace(',','',$data['price']);
        $price = round($price);
        unset($data['price']);

        $data = '{"productCode":"'.$data['productCode'].'","bookingDate":"'.$data['bookingDate'].'", "currencyCode":"'.$data['currencyCode'] .'", "ageBands":[{"bandId":1, "count":1}]}';
        
        $gradeCode = '';
        $data = $this->api->getTourCode($data);
        if(isset($data['data']) && !empty($data['data'])){
            $gradeCode = $data['data'][0]['gradeCode'];
            foreach ($data['data'] as $key => $value) {
                if($value['retailPrice'] == $price){
                    $gradeCode = $value['gradeCode'];
                }
            }
        }
        return \Response::json([
                'successful' => true,
                'gradeCode' => $gradeCode
        ]);
    }

    public function location(){
        $return = $this->api->location();
        $data = $return->getData()->data;

        DB::table('zViatorLocations')->delete();
        foreach ($data as $key => $value) {
            $add_data['destinationId'] = $value->destinationId;
            $add_data['destinationType'] = $value->destinationType;
            $add_data['destinationName'] = $value->destinationName;
            $add_data['iataCode'] = $value->iataCode;
            $add_data['latitude'] = $value->latitude;
            $add_data['longitude'] = $value->longitude;
            $add_data['timeZone'] = $value->timeZone;
            $add_data['parentId'] = $value->parentId;
            $add_data['lookupId'] = $value->lookupId;
            
            $added_data = ViatorLocation::create($add_data);
        }
        return $return;
    }


    public function hotels(){
        $data = Input::all();

        $path = '/booking/hotels/';
        Log::channel('viator_hotels')->info('Request : '.$this->url.$path.'?apiKey='.$this->api_key. json_encode($data));

        $response = $this->api->hotels($data);

        Log::channel('viator_hotels')->info('Response : '.json_encode($response));
        Log::channel('viator_hotels')->info('--------------------------------------------');

        return $response;
    }

    public function search_location(){
        $data = Input::all();
        $return = ViatorLocation::where('destinationName', $data['destinationName'])
                        ->where('destinationType', 'CITY')
                        ->first();
        if(!$return){
            $return = ViatorLocation::where('destinationName', $data['destinationName'])
                                ->where('destinationType', 'COUNTRY')
                                ->first();
        }   
        if(!$return){
            $return = ViatorLocation::where('destinationName', $data['destinationName'])
                                ->where('destinationType', 'REGION')
                                ->first();
        }                       
        return  \Response::json(array(
             'status' => 200,
             'data' => $return
        ));
    }
    
    public function getTourGrades(\Illuminate\Http\Request $request)
    {
        $data = Input::All();
        /*$data = [
          "ageBands" => [
            0 => [
              "bandId" => 1,
              "count" => 1
            ]
          ],
          "productCode" => "5353JAIPVTELEP",
          "bookingDate" => "2018-09-01",
          "currencyCode" => "AUD",
        ];*/
        $path = '/booking/availability/tourgrades';     
        Log::channel('viator_tourgrade')->info('Request : '.$this->url.$path.'?apiKey='.$this->api_key. json_encode($data));

        $grades = $this->api->getTourGrade($data);
        $grades_arr = json_decode($grades->content(),TRUE);
        if (!empty($grades_arr['data'])) {
            $domain = $request->header('domain');
            if ($domain) {
                $commissions = getDomainData($domain);
                if ($commissions['pricing'][0]['status'] == 0) {
                    $grades = updatePricing($grades,'activity','viator',$commissions['eroam'],$commissions['pricing']);
                }
            }
        }

        Log::channel('viator_tourgrade')->info('Response : '.json_encode($grades));
        Log::channel('viator_tourgrade')->info('--------------------------------------------');

        return $grades;        
    }
    
    public function checkDuration()
    {
        $datas = Input::all();
    }

    public function create_tours() // add by dhara for store viator api data to database
    {
        $datas = Input::all();
        
        $return = $this->api->activities($datas['data']);
        $allData = $return->getData()->data;
        
        foreach ($allData as $key => $value) {  

            if( $value->duration != "Flexible" && $value->duration != "Varies")
            {
                $allCategories = DB::table('tblCategoryDef')->whereIn('category_id', $value->catIds)->lists('category_name');           
                $categories = '';
                if($allCategories)
                {
                    $categories = implode(',', $allCategories);
                }


                $duration = $this->getDuration($value->duration);   
                if($duration['durationType'] != 'd' && $duration['duration'] != '')
                {
                    $send['code'] = $value->code;
                    $activityDetail = $this->api->activity_view($send);
                    $productDetail = $activityDetail->getData()->data;  
                    $check_code = ActivityNew::where('code',$value->code)->first();

                    if(count($check_code) == 0)
                    {
                        $data = [];
                        $data['name'] = $value->title;
                        $data['code'] = $value->code;
                        $data['activity_category_id'] = implode(',', $value->catIds);
                        $data['currency_id'] = 1;
                        $data['ranking'] = $value->rating;
                        $data['description'] = @$productDetail->description;
                        $data['duration'] = $duration['duration'];
                        $data['voucher_comments'] = @$productDetail->voucherRequirements;
                        $data['website'] = @$productDetail->webURL;
                        $data['city_id'] = $datas['city_id'];
                        $data['country_id'] = $datas['country_id'];
                        $data['destination_city_id'] = $datas['data']['destId'];
                        
                        if($data)
                        {
                            $activity       = ActivityNew::create($data);

                            $activity_id    = $activity->id;
                            $price          = $value->price;
                            $base_price     = ActivityBasePrice::create(['base_price' => $price]);

                            $markup         = ActivityMarkup::where(['is_default' => TRUE])->first();
                            $dataP          = [
                                        'activity_id'                   => $activity_id,
                                        'activity_supplier_id'          => 44,
                                        'activity_base_price_id'        => $base_price->id,
                                        'activity_markup_id'            => $markup->id, 
                                        'activity_markup_percentage_id' => $markup->activity_markup_percentage_id,
                                    ];
                            $season = ActivityPrice::create($dataP);
                            ActivityBasePrice::where('id','=',$base_price->id)->update(['activity_price_id' => $season->id]);

                            if($value->thumbnailHiResURL != '' && @getimagesize($value->thumbnailHiResURL))
                            {
                                $image_link     = $value->thumbnailHiResURL;//Direct link to image
                                $split_image    = pathinfo($image_link);
                                $count          = ActivityImage::where(['activity_id' => $activity_id, 'is_primary' => TRUE])->count(); 

                                // if there is NO existing image for the resource then set current uploaded image as primary
                                $is_primary = ($count) ? FALSE : TRUE;
                                $values     = array('activity_id' => $activity_id, 'is_primary' => $is_primary);
                                $filename   = $activity_id.'_'.date('YmdHis').'.'.$split_image['extension'];
                                // the path where the image is saved
                                $destination = 'uploads/activities/'.$activity_id;
                                // CREATE ORIGINAL IMAGE
                                $original_path = $destination.'/'.$filename;
                                File::makeDirectory( public_path($destination), 0775, FALSE, TRUE); // create directory if not existing yet
                                //$image->move($destination, $filename);  //comment by dhara
                                
                                file_put_contents($original_path, file_get_contents($image_link));

                                $values = array_merge($values, ['original' => $original_path]);

                                // CREATE THUMBNAIL IMAGE
                                $thumbnail_path = $destination.'/thumbnail/'.$filename; // set thumbnail path
                                $thumbnail_img  = Image::make($original_path); // create image intervention object from original image
                                $thumbnail_img  = resize_image_to_thumbnail( $thumbnail_img ); // resize; found in helpers.php
                                File::makeDirectory( public_path($destination).'/thumbnail', 0775, FALSE, TRUE);
                                $thumbnail_img->save($thumbnail_path); // store image 
                                $values = array_merge($values, ['thumbnail' => $thumbnail_path]); // thumbnail path to be stored in db

                                // CREATE SMALL IMAGE
                                $small_path = $destination.'/small/'.$filename; // set small path
                                $small_img  = Image::make($original_path); // create image intervention object from original image
                                $small_img  = resize_image_to_small( $small_img ); // resize; found in helpers.php
                                File::makeDirectory( public_path($destination).'/small', 0775, FALSE, TRUE);
                                $small_img->save($small_path); // store image 
                                $values = array_merge($values, ['small' => $small_path]); // small path to be stored in db      

                                // CREATE MEDIUM IMAGE
                                $medium_path = $destination.'/medium/'.$filename; // set medium path
                                $medium_img  = Image::make($original_path); // create image intervention object from original image
                                $medium_img  = resize_image_to_medium( $medium_img ); // resize; found in helpers.php
                                File::makeDirectory( public_path($destination).'/medium', 0775, FALSE, TRUE);
                                $medium_img->save($medium_path); // store image 
                                $values = array_merge($values, ['medium' => $medium_path]); // medium path to be stored in db   

                                // CREATE LARGE IMAGE
                                $large_path = $destination.'/large/'.$filename; // set large path
                                $large_img  = Image::make($original_path); // create image intervention object from original image
                                $large_img  = resize_image_to_large( $large_img ); // resize; found in helpers.php
                                File::makeDirectory( public_path($destination).'/large', 0775, FALSE, TRUE);
                                $large_img->save($large_path); // store image 
                                $values = array_merge($values, ['large' => $large_path]); // large path to be stored in db  

                                // STORE TO DATABASE
                                $data = ActivityImage::create( $values );
                            }
                        }
                    }
                }
            }
        }
    }

    
    public function getDuration($string)
    {
        $durationType = 'h'; //hour
        $duration     = '';
        if (strpos($string,'hours') !== false) {
            $durationType = 'h';
            $duration     = $string;
        }
        else if (strpos($string,'days') !== false) { // 3 days / 2 nights
            $explode = explode(" ",$string);
            $durationType = 'd';
            $duration     = $explode[0];
        }       
        else if (strpos($string,'day') !== false) { // 3 days / 2 nights
            $explode = explode(" ",$string);
            $durationType = 'd';
            $duration     = $explode[0];
        }
        else if(strpos($string, "minutes") !== false) // 75 minutes
        {
            $durationType = 'h';
            $duration     = $string;
        }
        else if (strpos($string,' to ') !== false) { // 24 to 72 hrs
            $explode = explode(" ",$string);
            if($explode)
            {
                $durationType = 'h';
                $duration     = $explode[2];
                if($explode[2] > 24)
                {
                    $durationType = 'd';
                    $duration     = round($explode[2]/24);
                }
                
            }
        }   
        $data['duration'] = $duration;
        $data['durationType'] = $durationType;
        return $data;
    }

    public function view_activity(\Illuminate\Http\Request $request){
        
        $data = Input::all();
        
        $result = [];
        $image_data = [];
        $response = [];
        //check if product has cache images

        $path = '/product';
        Log::channel('viator_products')->info('Request : '.$this->url.$path.'?apiKey='.$this->api_key. json_encode($data));
        $response = $this->api->activity_view($data);
        Log::channel('viator_products')->info('Response : '.json_encode($response));
        Log::channel('viator_products')->info('--------------------------------------------');

        $domain = $request->header('domain');

        $dataToCheck = json_decode($response->content(),TRUE);

        if ($domain && !empty($dataToCheck['data'])) {
            $commissions = getDomainData($domain);
            if ($commissions['pricing'][0]['status'] == 0) {
                $response = updatePricing($response,'activity','viator',$commissions['eroam'],$commissions['pricing']);
            }
        }

        $get_image_data = ViatorData::where('code',$data['code'])->first();
        if(!$get_image_data){
            //$response = $this->api->activity_view($data);
            $image_data['code'] = $data['code'];
            if ($domain) {
                $image_data['response']  = $response;
            }else{
                $image_data['response']  = $response->getContent();
            }
            ViatorData::create($image_data);
        }else{
            //$response = json_decode($get_image_data['response']);
            //$response = Response::json($response);
            if ($domain) {
                $image_data['response']  = $response;
            }else{
                $image_data['response']  = $response->getContent();
            }
            ViatorData::where('code', $data['code'])->update($image_data);
        }

        return $response;
    }

    public function save_category(){
        $return = $this->api->save_category();
        $response_data = $return->getContent();
        $response_data = json_decode($response_data);
        $data = $response_data->data;
        $added_data = [];

        ViatorCategory::truncate();

        foreach ($data as $key => $category) {
            $added_data['category_id'] = $category->id;
            $added_data['name'] = $category->groupName;
            $added_data['subcategories'] = json_encode($category->subcategories);
            ViatorCategory::create($added_data);
        }

        return $return;

    }



    public function calculate_price(){
        $data = Input::all();
       
    }

    public function get_location_by_destination_type( ){
        $where = [
            'destinationType' => Input::get('destinationType'),
            'country_code' => Input::get('countryCode')
        ];
        return ViatorLocation::where( $where )->get();
    }

    public function location_exists_by_country_code(){
        $result = [];
        $locations = ViatorLocation::where([ 'country_code' => Input::get('countryCode') ])->get();
        if( count( $locations ) > 0 ){
            foreach( $locations as $l ){
                array_push( $result, $l->destinationType );
            }
        }
        return $result;
    }

    public function get_tour_dates()
    {
        $datas = Input::all();      
        $return = $this->api->getTourDatesRates($datas);
        $allData = $return->getData()->data;
        return \Response::json(array(
            'status' => 200,
            'data' => $allData));
    }

    public function getvoucher()
    {
        $datas = Input::all();
        $return = $this->api->generate_voucher($datas['voucherKey']);
        //$return = $this->api->generate_voucher('1003873124:1fb0a0bc68e3b165289e127d3f4d9f6ee005e860c3c4b17b8fda0c87dc4d575a');
        return \Response::json(array(
            'status' => 200,
            'data' => $return));
    }

    public function getCategoryList($sType)
    {
        $oCategoryList = CategoryDef::where('category_from', $sType)->select('category_name','category_from_id')->get();
        return \Response::json(array(
            'status' => 200,
            'data' => $oCategoryList));
    }
}
