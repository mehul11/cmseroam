@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }

    .address{
        margin-left: 45px;
        font-family: sans-serif;
        font-size: 14px;
    }
</style>
@stop
@section('content')

<div class="content-container" >
    <h1 class="page-title">Location Details</h1> 

    @include('WebView::booking.review_booking_menu')
    <div class="box-wrapper">
	<p class="h4">Location Details</p>
	<hr>
		
		</br>
        <div class="row">
			<div class="col-sm-2">  Departure Date:  </div>
            <div class="col-sm-4"> {{$trip_details['itinerary_order']['from_date']}}</div>
			<div class="col-sm-2">Return Date: </div>
            <div class="col-sm-4"> {{$trip_details['itinerary_order']['to_date']}}</div>
			<div class="col-sm-2">  Number Of Nights:  </div>
            <div class="col-sm-4">{{$trip_details['itinerary_order']['total_days']}} </div>
		</div>
		</br>
	</div>  
    <div id="map1" style="height: 400px;"></div>
   
    <input type="hidden" id="search-input" value='{"_token":"ainMgKiuwraMwxrQbhbz4Y8IVrKhYU4boYtx2nPm","country":"605","city":"364","to_country":"605","to_city":"368","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"auto","searchValType2":null,"searchVal2":null,"start_location":"Delhi, India","end_location":"Jaipur, India","start_date":"2018-09-26","rooms":"1","travellers":1,"total_children":"0","num_of_pax":["1"],"firstname":{"1":{"1":"tet"}},"lastname":{"1":{"1":"tet"}},"dob":{"1":{"1":"03-05-1985"}},"age":{"1":{"1":"33"}},"child":[],"interests":[],"travel_preferences":null,"num_of_adults":[1],"num_of_children":[0],"pax":[[{"firstname":"tet","lastname":"tet","dob":"03-05-1985","age":"33","child":0}]],"childrens":0}'>
    <input type="hidden" id="search-session" value="{{json_encode($search_session)}}">
	<input type="hidden" id="map-data" value="{{json_encode($route)}}">
   
	</br>
	<div class="m-t-20 row">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="row">
				<div class="col-sm-6">
					<a href="{{ route('booking.booking-listing-detail',['nItenaryId'=>$nBookId]) }}" class="btn btn-primary btn-block">Previous</a>
				</div>
				<div class="col-sm-6">
				   <a href="{{ route('booking.booking-product-detail',['nItenaryId'=>$nBookId]) }}" class="btn btn-primary btn-block">Next</a>
				</div>
			</div>
		</div>
    </div>
	</div>
   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>
    <script src="{{url('js/map/eroam.js')}}"></script>
    <script src="{{url('js/map/markerlabel.js')}}"></script>
    <script src="{{url('js/map/eroam-map_managetrip.js')}}"></script>
	<script type="text/javascript">
        $(document).ready(function() {
            tripHideShow();
            tripHotelHideShow();
        });

        function tripHideShow() {
            $('.trip-viewmore').click(function() {
                var href_value = $(this).attr('data-id');
                $('#' + href_value).toggle();
                $('#' + href_value).toggleClass("open");
                $(this).toggleClass("active");
            });
        }

        function setupLabel() {
            if ($('.label_radio input').length) {
                $('.label_radio').each(function() {
                    $(this).removeClass('r_on');
                });
                $('.label_radio input:checked').each(function() {
                    $(this).parent('label').addClass('r_on');
                    var inputValue = $(this).attr("value");
                    //alert(inputValue);
                    $("." + inputValue).addClass('mode-block');
                    $("." + inputValue).siblings().removeClass('mode-block');
                });
            };

            if ($('.label_check input').length) {
                $('.label_check').each(function() {
                    $(this).removeClass('c_on');
                });
                $('.label_check input:checked').each(function() {
                    $(this).parent('label').addClass('c_on');
                });
            };

        };
        $(window).load(eMap.init);

        function map() {
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);

            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York

                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{
                        "featureType": "administrative",
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#444444"
                        }]
                    }, {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#f2f2f2"
                        }]
                    }, {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    }, {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [{
                            "saturation": -100
                        }, {
                            "lightness": 45
                        }]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "simplified"
                        }]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "labels.icon",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    }, {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    }, {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#46bcec"
                        }, {
                            "visibility": "on"
                        }]
                    }]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map1');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(40.6700, -73.9400),
                    map: map,
                    title: 'eRoam!'
                });
            }
        }

        function tripHotelHideShow(){
            $('.tourTripName').click(function(){
                var href_value = $(this).attr('data-id');
                $('#'+href_value).toggle();
            });
        }
   </script>
</script>
@stop
    
