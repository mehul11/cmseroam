@foreach ($oUserList as $aUser)		
<?php $names =split_name($aUser->name); ?>		
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aUser->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aUser->id;?>" value="<?php echo $aUser->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="#">
                {{ $aUser->id }}
            </a>
        </td>
        <td>{{ !empty(current($names)) ? current($names):'-' }}</td>
        <td>{{ !empty(last($names) ) ? last($names) :'-'}}</td>
        <td>
        {{ Auth::user()->name }}</td>
        <td>{{ date('d/m/Y',strtotime($aUser->created_at)) }}</td>
        <td class="text-center">
            <a href="{{URL::to('users/edit-customer/'.$aUser->id)}}" title="Edit User"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
            <a href="{{URL::to('users/passenger-details/'.$aUser->id)}}" title="PAX Details"><i class="fa fa-eye"></i></a>

        </td>	
    </tr> 
@endforeach