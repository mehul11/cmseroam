<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zcities'))
        {
            Schema::create('zcities', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name',255);
                $table->integer('region_id')->unsigned()->nullable();
                $table->string('goehash',20)->nullable();
                $table->string('optional_city',255)->nullable();
                $table->text('description')->nullable();
                $table->string('small_image')->nullable();
                $table->string('row_id',255)->nullable();
                $table->integer('country_id')->unsigned()->nullable();
                $table->integer('default_nights')->nullable();
                $table->tinyInteger('is_disabled');
                $table->string('airport_codes',255)->nullable();
                $table->integer('timezone_id')->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('region_id')->references('id')->on('zregions');
                $table->foreign('country_id')->references('id')->on('zcountries');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zcities');
    }
}
