<div class="row">
  <div class="col-sm-12">
    <div class="form-group">
      <label class="label-control">Notes</label>
      
      <textarea name="comment" name="notes" id="{{$itenary_order_id}}" class="form-control" placeholder="Notes"></textarea>
    </div>
      <span class="error-custom" style="color:red;"></span>
  </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <input type="button" value="Save" class="btn btn-primary btn-block updateItenaryButton">
        </div>
    </div>
</div>
<script type="text/javascript"> 
	$(".updateItenaryButton").click(function(){ 
	var itenary_order_id = "{{$itenary_order_id}}";
	var buttonType = "{{$buttonType}}";
    var notes = $.trim($("#"+itenary_order_id).val());
    if(notes == ""){
    	$(".error-custom").html("Please enter notes.");
    	return false;
    }
    showLoader();
    $.ajax({
        method: 'post',
        url: '{{ url('update-revised-itenary') }}',
        data: {
        _token: '{{ csrf_token() }}',
                lagDetailsId: itenary_order_id,
                notes:notes
        },
        success: function(response) {
        	if(buttonType=="activity"){
            	$(".addActivityDiv_"+itenary_order_id).html("<b>Additional Notes:</b> "+ response);
        	}if(buttonType=="transport"){
            	$(".addTransportDiv_"+itenary_order_id).html("<b>Additional Notes:</b> "+ response);
        	}if(buttonType=="hotel"){
            	$(".addHotelDiv_"+itenary_order_id).html("<b>Additional Notes:</b> "+ response);
        	}
            hideLoader();
        }
    });
}); 
</script>