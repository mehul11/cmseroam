<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('special_offers'))
        {
        Schema::create('special_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('offer_name');
            $table->string('offer_type');
            $table->string('inventory_type');
            $table->string('product_name');
            $table->string('client');
            $table->string('selling_start_date');
            $table->string('selling_end_date');
            $table->string('travel_start_date');
            $table->string('travel_end_date');
            $table->float('original_price');
            $table->enum('children_discount',['Yes','No']);
            $table->float('discount_offer');
            $table->float('discount_price');
            $table->float('children_discount_offer');
            $table->float('children_discount_price');
            $table->text('offer_description');
            $table->string('term_option');
            $table->string('additional_services');
            $table->string('terms_n_conditions');
            $table->string('cost_per_night');
            $table->string('original_nights');
            $table->string('free_nights');
            $table->string('fee');
            $table->enum('status',['Pending','Active','Expired','Deleted']);
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_offers');
    }
}
