<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Log;
use function GuzzleHttp\json_encode;

class BusBudApiController extends Controller
{

    public function __construct(){
        $this->api_Token       = "PARTNER_IwaIR8QCQtujtyNa4cQAVw";
        $this->client        = new Client();
        $this->header        = [
            "X-Busbud-Token"      => $this->api_Token,
            "version"  => 2,
            "Content-Type" => "application/json",
            "Accept"       => "application/json"
        ];
        $this->url           = 'https://napi-preview.busbud.com/';
        //$this->url_content   = 'https://api.hotelbeds.com/hotel-content-api/1.0/';
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getAllTransport()
    {
        $data = Input::all();
        $OriginLocationCode = 'u09tvm';
        $DestinationLocationCode = 'dre2em';
        //------Test data----//
        // $data['DepartureDate'] = '2018-09-15';
        // $data['OriginLocationCode'] = 'u05kq2';
        // $data['DestinationLocationCode'] = 'u09tvm';
        // $data['adult'] = '1';
        // $data['child'] = '0';
        // $data['domain'] = 'www.supertravel.com';
        //-------------------//

        if (isset($data['domain'])) {
          $domain = $data['domain'];
          if ($domain) {
              $commissions = getDomainData($domain);
          }
        }
        $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2');
        // Note: when using the POST or PUT methods, you must add the header "Content-Type: application/json"
        if(isset($data["child"]) && $data["child"] !="0"){ 
                $where = "?adult=".$data["adult"]."&child=".$data["child"]."&child_ages=".$data["childAge"]."&currency=AUD";
        }
        else{
                $where = "?adult=".$data["adult"]."&child=".$data["child"]."&currency=AUD";
        }
        $url = $this->url.'x-departures/'.$data["OriginLocationCode"].'/'.$data["DestinationLocationCode"].'/'.$data["DepartureDate"].$where;
        $OriginLocationCode = $data["OriginLocationCode"];
        $DestinationLocationCode = $data["DestinationLocationCode"];
        $DepartureDate = $data["DepartureDate"];

        Log::channel('busbud')->info('Request : '.$url);
        Log::channel('busbud')->info('--------------------------------------------');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $url);

        // Workaround to access SSL server and slide in through side door
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // Hide curl response
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       
        $result = curl_exec($curl);
        
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);

        curl_close($curl);
        $data =  json_decode($result);

        Log::channel('busbud')->info('Response : '.$result);
        Log::channel('busbud')->info('--------------------------------------------');

        $transport = array();
        
        if($data->departures){

            foreach($data->operators as $val) {
                $data1[$val->id] = $val->name;
            }

            foreach($data->departures as $key => $value) { 

                    $d = ['departure_time' => date('G:i',strtotime($value->departure_time)),
                                'arrival_time' => date('G:i',strtotime($value->arrival_time)),
                                'price' => $value->prices->total/100,
                                'timeInHours' => $this->convertToHoursMins($value->duration, '%02d hr(s) %02d min(s)'),
                                'transportTypeName' => $data1[$value->operator_id],
                                'transportId' => $value->id,
                                'duration' => $value->duration,
                                'transportClass' => $value->class,
                                'currency' => $value->prices->currency,
                                'departure_time1' => $value->departure_time,
                                'arrival_time1' => $value->arrival_time
                          ];
                    $transport[$key] =  $d;
             }
          
            
            if (!$data->complete) { 
              $transport = $this->pollCall($transport, $OriginLocationCode, $DestinationLocationCode, $DepartureDate, $where,0);
              sleep(2);
             
            }
            if (isset($commissions) && $commissions['pricing'][0]['status'] == 0) {
              $transport = updateTransportPricing($transport,'transport','busbud',$commissions['eroam'],$commissions['pricing']);
            }
            
            return \Response::json(array(
                    'status' => 'sucess',
                    'data' => $transport));
        }else{
          sleep(2);
         
          if (isset($commissions) && $commissions['pricing'][0]['status'] == 0) {
            $transport = updateTransportPricing($transport,'transport','busbud',$commissions['eroam'],$commissions['pricing']);
          }
          $transport = $this->pollCall($transport, $OriginLocationCode, $DestinationLocationCode, $DepartureDate, $where,0);

          return \Response::json(array(
                    'status' => 'sucess',
                    'data' => $transport));
        }
    }

    public function pollCall($transport,$OriginLocationCode,$DestinationLocationCode,$date,$where,$count=0){
      if (++$count > 10){return [];}
      
      if(count($transport) == 0){
        $pushData = 'No';
      }else{
        $pushData = 'Yes';
      }
      //echo $pushData;
      $urll  = 'https://napi-preview.busbud.com/';
      $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2');
      $url = $urll.'x-departures/'.$OriginLocationCode.'/'.$DestinationLocationCode.'/'.$date.'/poll'.$where;

      Log::channel('busbud')->info('Request : '.$url);
      Log::channel('busbud')->info('--------------------------------------------');
     
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $result = curl_exec($curl);
      Log::channel('busbud')->info('Response : '.$result);
      Log::channel('busbud')->info('--------------------------------------------');

      curl_close($curl);
      $data =  json_decode($result);  
      if($data->departures) { 

          foreach ($data->operators as $val) {
              $data1[$val->id] = $val->name;
          }

          foreach ($data->departures as $key => $value) {

              $d = ['departure_time' => date('G:i', strtotime($value->departure_time)),
                  'arrival_time' => date('G:i', strtotime($value->arrival_time)),
                  'price' => $value->prices->total / 100,
                  'timeInHours' => $this->convertToHoursMins($value->duration, '%02d hr(s) %02d min(s)'),
                  'transportTypeName' => $data1[$value->operator_id],
                  'transportId' => $value->id,
                  'duration' => $value->duration,
                  'transportClass' => $value->class,
                  'currency' => $value->prices->currency,
                  'departure_time1' => $value->departure_time,
                  'arrival_time1' => $value->arrival_time
              ];
              $transport[$key] = $d;
          }

          if (!$data->complete) { 
              $transport = $this->pollCall($transport, $OriginLocationCode, $DestinationLocationCode, $date, $where,$count);
          }
      }else{
        $transport = $this->pollCall($transport, $OriginLocationCode, $DestinationLocationCode, $date, $where,$count);
      }
      return $transport;  
  }

   public function createBusBudCart(){
      $data = Input::all(); 

      $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2');
      if(isset($data["child"]) && $data["child"] !="0"){       
        $params = "origin_city=".$data['originlocationcode']."&destination_city=".$data['destinationlocationcode']."&outbound_date=".$data['departuredate']."&adult=".$data['travellers']."&child=".$data["child"]."&child_ages=".$data["childAge"]."&senior=0&departure_id=".$data['departureId'];
}else{$params = "origin_city=".$data['originlocationcode']."&destination_city=".$data['destinationlocationcode']."&outbound_date=".$data['departuredate']."&adult=".$data['travellers']."&child=".$data["child"]."&senior=0&departure_id=".$data['departureId'];}
        
        $url = 'https://napi-preview.busbud.com/cart';
        
        Log::channel('busbud')->info('Request : '.'API='.$url.' '.'params='.json_encode($params));
        Log::channel('busbud')->info('--------------------------------------------');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$params);
        // Workaround to access SSL server and slide in through side door
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // Hide curl response
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result1 = curl_exec($curl);


        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($result1, 0, $header_size);
        $body = substr($result1, $header_size);

        curl_close($curl);
        $data1 =  json_decode($result1);

        if(isset($data1->error)){
             $dataError = $data1->error;
             \Log::error( '----------------------------------------------' );
                    \Log::error( [
                       'headers' => $header,
                       'body' => $body
                   ] );

                Log::channel('busbud')->info('Response : '.json_encode($dataError));
                Log::channel('busbud')->info('--------------------------------------------');  

            return \Response::json([
                'result' => 'No',
                'data' => $dataError ]);
        }else{
            $cartId = $data1->data->id;
            $passengers = $data1->data->passengers;
            Log::channel('busbud')->info('Response : '.json_encode($passengers));
            Log::channel('busbud')->info('--------------------------------------------');  
            return \Response::json([
                'result' => 'Yes',
                'cartId' => $cartId,
                'passengers' => $passengers  ]);
                  
        }
   }

    public function bookingProcess(){
        $data = Input::all();       
        $cartId = $data['cartId'];
        $passangerArray = $data['passangerArray'];
        $cartUpdate=$data['cartData'];   
        $passengers = array();      
        $qstArr=['dob','government_id','gender','nationality','profession','marital_status','outbound_breakfast','outbound_lunch','outbound_supper','return_breakfast','return_lunch','return_supper'];
        $payload=[];
        foreach($qstArr as $key=>$val){
            if(in_array($val,$cartUpdate)){
                array_push($payload,$val);
            }
        }
        for($j=0;$j<count($passangerArray);$j++){
           if($j == 0){
               $questionarr=[];
                    foreach($payload as $key=>$val)
                    {
                            switch($val)
                            {
                                case 'dob':
                                $val=isset($data['passenger_dob'][$j]) ? $data['passenger_dob'][$j] : NULL;
                                $questionarr['dob']=$val;
                                break;
                                case 'gender':
                                $val=isset($data['passenger_gender'][$j]) ? $data['passenger_gender'][$j] : NULL;
                                $questionarr['gender']=$val;
                                break;
                                case 'nationality':
                                $val=isset($data['passenger_country'][$j]) ? $data['passenger_country'][$j]:null;
                                $questionarr['nationality']=$val;
                                break;
                                case 'profession':
                                $val=isset($data['profession'][$j]) ? $data['profession'][$j]:null;
                                $questionarr['profession']=$val;
                                break;
                                case 'government_id':
                                $val=isset($data['government_id'][$j]) ? $data['government_id'][$j]:null;
                                $questionarr['government_id']=$val;
                                break;
                                case 'marital_status':
                                $val=isset($data['marital_status'][$j]) ? $data['marital_status'][$j]:null;
                                $questionarr['marital_status']=$val;
                                break;
                                case 'outbound_breakfast':
                                $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                                $questionarr['outbound_breakfast']=$val;
                                break;
                                case 'outbound_lunch':
                                $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                                $questionarr['outbound_lunch']=$val;
                                break;
                                case 'outbound_supper':
                                $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                                $questionarr['outbound_supper']=$val;
                                break;
                                case 'return_breakfast':
                                $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                                $questionarr['return_breakfast']=$val;
                                break;
                                case 'return_lunch':
                                $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                                $questionarr['return_lunch']=$val;
                                break;
                                case 'return_supper':
                                $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                                $questionarr['return_supper']=$val;
                                break;
                            }

                    }
             $passengers['passengers'][$j] = array(
                                'id' => $passangerArray[$j]['id'],
                                'first_name' => isset($data['passenger_first_name'][$j]) ? $data['passenger_first_name'][$j] : NULL,
                                'last_name' => isset($data['passenger_last_name'][$j]) ? $data['passenger_last_name'][$j] : NULL,
                                'phone_number' => isset($data['passenger_contact_no']) ? $data['passenger_contact_no'] : NULL,
                                'questions' =>$questionarr,
                                'passenger_category' => 'adult',
                                'source_passenger_category' => 'adult',
                                'age' => NULL,
                                'outbound_selected_seat' => NULL,
                                'return_selected_seat' => NULL,
                                'primary_contact' => true,
                                'email' => $data['passenger_email'],
                            );
           }else{
            $questionarr2=[];
            foreach($payload as $key=>$val)
            {
                    switch($val)
                    {
                        case 'dob':
                        $val=isset($data['passenger_dob'][$j]) ? $data['passenger_dob'][$j] : NULL;
                        $questionarr2['dob']=$val;
                        break;
                        case 'gender':
                        $val=isset($data['passenger_gender'][$j]) ? $data['passenger_gender'][$j] : NULL;
                        $questionarr2['gender']=$val;
                        break;
                        case 'nationality':
                        $val=isset($data['passenger_country'][$j]) ? $data['passenger_country'][$j]:null;
                        $questionarr2['nationality']=$val;
                        break;
                        case 'profession':
                        $val=isset($data['profession'][$j]) ? $data['profession'][$j]:null;
                        $questionarr2['profession']=$val;
                        break;
                        case 'government_id':
                        $val=isset($data['government_id'][$j]) ? $data['government_id'][$j]:null;
                        $questionarr2['government_id']=$val;
                        break;
                        case 'marital_status':
                        $val=isset($data['marital_status'][$j]) ? $data['marital_status'][$j]:null;
                        $questionarr2['marital_status']=$val;
                        break;
                        case 'outbound_breakfast':
                        $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                        $questionarr2['outbound_breakfast']=$val;
                        break;
                        case 'outbound_lunch':
                        $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                        $questionarr2['outbound_lunch']=$val;
                        break;
                        case 'outbound_supper':
                        $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                        $questionarr2['outbound_supper']=$val;
                        break;
                        case 'return_breakfast':
                        $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                        $questionarr2['return_breakfast']=$val;
                        break;
                        case 'return_lunch':
                        $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                        $questionarr2['return_lunch']=$val;
                        break;
                        case 'return_supper':
                        $val=isset($data['meal'][$j]) ? $data['meal'][$j]:null;
                        $questionarr2['return_supper']=$val;
                        break;
                    }

            }  
            $passengers['passengers'][$j] = array(
                                'id' => $passangerArray[$j]['id'],
                                'first_name' => isset($data['passenger_first_name'][$j]) ? $data['passenger_first_name'][$j] : NULL,
                                'last_name' => isset($data['passenger_last_name'][$j]) ? $data['passenger_last_name'][$j] : NULL,
                                'phone_number' => isset($data['passenger_contact_no']) ? $data['passenger_contact_no'] : NULL,
                                'questions' =>$questionarr2,
                                'passenger_category' =>  isset($data['passenger_type'][$j]) ? $data['passenger_type'][$j] : NULL,
                                'source_passenger_category' =>  isset($data['passenger_type'][$j]) ? $data['passenger_type'][$j] : NULL,
                                'age' => isset($passangerArray[$j]['age']) ? $passangerArray[$j]['age'] : NULL,
                                'outbound_selected_seat' => NULL,
                                'return_selected_seat' => NULL,
                                'primary_contact' => false,
                                'email' => '',
                            );
           }

    }
            $data['passenger'] =  $passengers;
            $data_json = json_encode($data['passenger']);
            $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2','Content-type:application/json');          
            $url = 'https://napi-preview.busbud.com/cart/'.$cartId;
            Log::channel('busbud')->info('Request : '.'API='.$url.' '.'update_data='.$data_json);
            Log::channel('busbud')->info('--------------------------------------------');

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_URL, $url);
            //curl_setopt($curl, CURLOPT_PUT, true);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($curl, CURLOPT_POSTFIELDS,$data_json);
            // Workaround to access SSL server and slide in through side door
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            // Hide curl response
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result2 = curl_exec($curl);
          
            curl_close($curl);
            $data2 =  json_decode($result2);
            if(isset($data2->error)){
             $dataError = $data2->error;
            Log::channel('busbud')->info('Response : '.json_encode($dataError));
            Log::channel('busbud')->info('--------------------------------------------');

                return \Response::json([
                    'result' => 'No',
                    'data' => $dataError ]);
            }else{     
             // functionality for final payment                                                           
                $header = array('X-Busbud-Token: PARTNER_GjUXjuHSRjitXRG7icgj-w','version:2');                
                $params1 = "email_opt_in=false&payment_type=iou";
                $url = 'https://napi-preview.busbud.com/cart/'.$cartId.'/purchase'; 

                Log::channel('busbud')->info('Request : '.$url);
                Log::channel('busbud')->info('--------------------------------------------');

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POSTFIELDS,$params1);
                // Workaround to access SSL server and slide in through side door
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                // Hide curl response
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_VERBOSE, 1);
               
                $result3 = curl_exec($curl);
    
                curl_close($curl);
                $data3 =  json_decode($result3);
                if(isset($data3->error)){
                 $dataError = $data3->error;
                 Log::channel('busbud')->info('Response : '.json_encode($dataError));
                 Log::channel('busbud')->info('--------------------------------------------');
                    return \Response::json([
                        'result' => 'No',
                        'data' => $dataError ]);
                 }else{
                    Log::channel('busbud')->info('Response : '.$result3);
                    Log::channel('busbud')->info('--------------------------------------------');
                   return \Response::json([
                    'result' => 'Yes',
                    'data' => $data3 ]);
                   
                }
            }
    }

    function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    public function response_format($success, $data, $error){
		if(is_array($data)){
			if(count($data) < 1)
				$data = NULL;
		}
		if(is_array($error)){
			if(count($error) < 1)
				$error = NULL;
		}		
		$result = array(
					'success' => $success,
					'data' => $data,
					'error' => $error
					);
		return \Response::json( $result );
	}
}
