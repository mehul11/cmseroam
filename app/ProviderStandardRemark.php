<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderStandardRemark extends Model
{
    protected $fillable = [
        'provider_id','standard_desc'
    ];
    protected $table = 'tblproviderstandardremarks';
    protected $primaryKey = 'standard_remarks_id';
    public $timestamps = false;
}
