@foreach ($oHotelList as $aHotel)	
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aHotel->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aHotel->id;?>" value="<?php echo $aHotel->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="{{ route('acomodation.hotel-create',['nIdHotel' => $aHotel->id]) }}">
                {{ $aHotel->name }}
            </a>
        </td>
        <td>{{ $aHotel->eroam_code }}</td>
        <td>{{ $aHotel->supplier_name }}</td>
        <td> {{ $aHotel->address_1 }} </td>
        <td>
            @php
                $domain_array = [];
                if($aHotel->domains){
                    $domain_array = explode(',',$aHotel->domains);
                    foreach($domain_array as $key1=>$value1){
                        echo domianName($value1)->name.'<br/>';
                    }
                }
            @endphp
        </td>
        <td> @if(($aHotel->is_publish)=='1'){{ trans('messages.publish') }}@endif @if(($aHotel->is_publish)!='1'){{ trans('messages.unpublish') }}@endif</td>
        <td>
        @if(($aHotel->is_publish)=='1')
        <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.unpublish_btn') }}" onclick="callPublishRecord(this,'{{ route('acomodation.hotel-publish',['HotelId'=> $aHotel->id]) }}')">
        @endif
        @if(($aHotel->is_publish)!='1')
        <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.publish_btn') }}" onclick="callPublishRecord(this,'{{ route('acomodation.hotel-publish',['HotelId'=> $aHotel->id]) }}')">
        @endif   
        </td>
    </tr> 
@endforeach