<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblproviderspecialnotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tblproviderspecialnotes'))
        {
        Schema::create('tblproviderspecialnotes', function (Blueprint $table) {
            $table->increments('special_note_id');
            $table->integer('provider_id');
            $table->longText('special_desc')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblproviderspecialnotes');
    }
}
