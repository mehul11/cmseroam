@foreach ($oHotelList as $aHotel)               
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aHotel->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aHotel->id;?>" value="<?php echo $aHotel->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="{{ route('acomodation.hotel-season-create',[ 'nIdHotelSeason' => $aHotel->id ])}}">
                {{ $aHotel->name }}
            </a>
        </td>
        <td>{{ $aHotel->erom_code }}</td>
        <td>{{ $aHotel->supplier_name }}</td>
        <td>{{ $aHotel->from }}</td>
        <td>{{ $aHotel->to }}</td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <a href="{{ route('acomodation.hotel-season-create',[ 'nIdHotelSeason' => $aHotel->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('acomodation.hotel-season-delete',['nIdSeason'=> $aHotel->id]) }}','{{ trans('messages.delete_label')}}')">
            </div>
        </td>
    </tr> 
@endforeach