@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.manage_list_title', ['name' => 'Country']) }}</h1>

    <div class="row">
        <div class="small-12 small-centered columns delete-box hidden"></div> 
    </div>
    @if(Session::has('message'))
        <div class="small-12 small-centered columns success_message">{{ Session::get('message') }}</div>
        <br>
    @endif
    <div class="box-wrapper">
<!--        <a href="" class="plus-icon" title="Add">
            <i class="icon-plus"></i>
        </a>-->
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                    <input type="text" class="form-control m-t-10" placeholder="Search {{ trans('messages.country') }}" name="search_str" value="{{ $sSearchStr }}">
                    
                </div>
            </div>

            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                <select name="search-by" class="form-control m-t-10 search_by">
                    <option value="name" {{ $sSearchBy == 'name' ? 'selected' : '' }}>Name</option>
                    <option value="code" {{ $sSearchBy == 'code' ? 'selected' : '' }}>Code</option>
                </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <button class="btn btn-default btn-sm" type="submit" onclick="getMoreListing(siteUrl('common/country-list'),event,'table_record');"><i class="icon-search-domain"></i></button>
            </div>
        </div>
        <div class="m-t-30">
            <label>{{ trans('messages.show_record') }}</label>
            <select class="select-entry" name="show_record" onchange="getMoreListing(siteUrl('common/country-list?page=1'),event,'table_record');">
                <option value="10" {{ ($nShowRecord == 10) ? 'selected="selected"' : '' }}>10</option>
                <option value="20" {{ ($nShowRecord == 20) ? 'selected="selected"' : '' }}>20</option>
                <option value="30" {{ ($nShowRecord == 30) ? 'selected="selected"' : '' }}>30</option>
                <option value="40" {{ ($nShowRecord == 40) ? 'selected="selected"' : '' }}>40</option>
            </select>
            <label>{{ trans('messages.entries') }}</label>
        </div>
        <div class="table-responsive m-t-20 table_record">
           
            @include('WebView::common._country_list_ajax')
      
        </div>
    </div>

</div>
@stop
@section('custom-js')
<script type="text/javascript">
$(document).on('ready',function(){
	$('.switch1-state1').bootstrapSwitch();
	$(document).on('tbody switchChange.bootstrapSwitch','.switch1-state1', function (event, state) {
            switchChange(state,this);
	});
    $('.update-region-btn').click(function(event) {
        event.preventDefault();

        var box = $(this).parent().siblings('.update-region-box');
        var span = $(this).parent();
        var countryId = span.data('country-id');
        var regionId = span.data('region-id');
        var regions = JSON.parse('[{"id":"1","name":"Asia\/Pacific","description":null,"created_at":null,"updated_at":null,"deleted_at":null},{"id":"3","name":"UK\/Europe","description":null,"created_at":null,"updated_at":null,"deleted_at":null},{"id":"5","name":"Africa\/Middle East","description":null,"created_at":null,"updated_at":null,"deleted_at":null},{"id":"7","name":"USA\/Canada","description":null,"created_at":null,"updated_at":null,"deleted_at":null},{"id":"9","name":"Latin America","description":null,"created_at":null,"updated_at":null,"deleted_at":null}]');
        var options;

        // HIDE
        span.hide();

        $.each(regions, function(key, value) {
            var selected = value.id == regionId ? 'selected' : '';
            options += '<option value="'+value.id+'" '+selected+'>'+value.name+'</option>';
        });
        box.html(
            '<div class="row">'+
                '<div class="large-8 columns">'+
                    '<select class="region-select">'+options+'</select>'+
                '</div>'+
            '</div>'+
            '<div class="row">'+
                '<div class="large-8 columns">'+
                    '<a href="#" data-country-id="'+countryId+'" class="button tiny save-update-region-btn">Save</a> '+
                    '<a href="javaScript:void(0);" class="button tiny alert cancel-update-region-btn">Cancel</a>'+
                '</div>'+
            '</div>'
        ).hide().fadeIn(200);
    });
    $('body').on('click', '.save-update-region-btn', function(event) {
        event.preventDefault();
        var countryId = $(this).data('country-id');
        var regionId = $(this).parent().parent().prev().find('select.region-select').val();
        var span = $(this).parent().parent().parent().prev();
        var cancelBtn = $(this).next('.cancel-update-region-btn');

        $.ajax({
            method: 'post',
            url: '{{ route('common.update-region') }}',
            data: {
                country_id: countryId,
                region_id: regionId
            },
            success: function(response) {
                span.find('.region-name').html(response).effect('highlight', {color: '#91E3AB'}, 300);
                span.data('region-id', regionId)
                cancelBtn.click();
            }
        });
    });
    $('body').on('click', '.cancel-update-region-btn', function(event) {
        event.preventDefault();
        var box = $(this).parent().parent().parent('.update-region-box');
        var span = box.prev('span');
        box.html('');
        span.show();
    });
});
function switchChange(state,oEle)
{
    var id = $(oEle).data('id');
    var checked = (state === true) ? 1 : 0;
    var label = $(this).next('label');
    showEroamSwitch("{{ route('common.country-switch') }}"+'/'+id , checked);
}
function getCountrySort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    getMoreListing(siteUrl('common/country-list'),event,'table_record');
}

$(document).on('click',".cmp_check",function () { 

    if($('.cmp_check:checked').length == $('.cmp_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
$(document).on('click','.label_check',function(){
    setupLabel();
});
</script>
@stop