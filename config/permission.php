<?php

return [

    "admin"=>[
        "accounts_reporting",
        "booking_management",
        "currency_management",
        "administration",
        "geo_data",
        "label", 
        "client_onboarding",
        "transport_management",
        "route_plans_create_itinerary",
        "tour_management",
        "offer_place",     
        "special_offer",
        "support_faq",
        "licensee",
        //"agent",
        "product",
        "brand",
        "admin",
        //"customer",
        "user_management",
        "inventory_market_place",
        "accomodation_management",
        "acc_manage_suppliers",
        "acc_manage_labels_mapping",
        "acc_manage_seasons",
        "acc_manage_prices",
        "acc_manage_mark_up",
        "acc_manage_room_types",
        "acc_aot_location",
        "acc_aot_supplier",
        "acc_expedia_hotel_mapping",
        "transport_management",
        "t_manage_transport",
        "t_manage_suppliers",
        "t_manage_operator",
        "t_manage_season",
        "t_transport_type",
        "t_manage_mark_up",
        "t_manage_airlines",
        "a_activity_management",
        "a_manage_activities",
        "a_manage_suppliers",
        "a_manage_seasons",
        "a_manage_mark_up",
        "a_activity_operator"

    ],
    "eroamProduct"=>[
        "geo_data",      
        "support_faq", 
        "accomodation_management",
        "inventory_market_place",
        "acc_manage_seasons",
        "t_manage_transport",
        "t_manage_season",
        "transport_management",
        "a_activity_management",
        "a_manage_activities",
        "a_manage_seasons",
        "user_management"

    ],
    "brand"=>[
        "accounts_reporting",
        "booking_management",
        "administration",
        "brand",
        "geo_data",
        "support_faq", 
        "accomodation_management",
        "inventory_market_place",
        "acc_manage_seasons",
        "t_manage_transport",
        "t_manage_season",
        "transport_management",
        "a_activity_management",
        "a_manage_activities",
        "a_manage_seasons",
        "user_management"     
    ],
    "product"=>[
        "geo_data",      
        "support_faq", 
        "accomodation_management",
        "inventory_market_place",
        "acc_manage_seasons",
        "t_manage_transport",
        "t_manage_season",
        "transport_management",
        "a_activity_management",
        "a_manage_activities",
        "a_manage_seasons",
        "user_management"     
    ],
  
    "licensee"=>[
        "accounts_reporting",
        "booking_management",
        "administration",
        "brand",
        "geo_data",
        "support_faq", 
        "accomodation_management",
        "inventory_market_place",
        "acc_manage_seasons",
        "t_manage_transport",
        "t_manage_season",
        "transport_management",
        "a_activity_management",
        "a_manage_activities",
        "a_manage_seasons",
        "user_management"     
    ],

    "agent"=>[
        "accounts_reporting",
        "booking_management",
        "accomodation_management",
        "inventory_market_place",
        "acc_manage_seasons",
        "t_manage_transport",
        "t_manage_season",
        "transport_management",
        "a_activity_management",
        "a_manage_activities",
        "a_manage_seasons",
        "user_management"
    ],
    
    "customer"=>[
        "accounts_reporting",
        "booking_management",
        "currency_management",
        "Administration",
        "geo_data",
        "label", 
        "offer_place",     
        "special_offer"
    ],

    "consultant" =>[


    ],

    "supplier" =>[

        "offer_place",     
    ],
  
]


?>