<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Agent extends Model {

    protected $table = 'zagents';
    protected $fillable = ['user_id', 'address', 'contact_number', 'image_url','agent_commission_percentage_id'];

    protected $dates = ['deleted_at']; 
}
