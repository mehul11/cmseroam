<?php
namespace App\Services\RoomerFlex\Resources;

class ServiceDescriptionV3
{   

    /*
    Start request_token  first step
    send all required details in order to generate a unique protection token
    */
    static function getRequestTokenOperation() {
        return [
            "httpMethod" => "POST",
            "uri" => "/api/protection/request_token",
            "summary" => "Get Package Request Token",
            "responseModel" => "PackageResponse",
            "parameters" => [
                "package" => json_decode(file_get_contents(__DIR__.'/Schema/Package.json'),true)
            ]
        ];
    }

    static function getRequestTokenResponse() {
        return [
            'type' => 'object',
            "extends"=> "BaseResponse",
            'properties' => [
                'success' => [
                    'type' => 'boolean'
                ],
                'lh-token' => [
                    'type' => 'string'
                ],
                'lh-refund-rate' => [
                    'type' => 'string'
                ],
                'lh-fee' => [
                    'type' => 'float'
                ],
                'lh-fee-per-night' => [
                    'type' => 'string'
                ],
                'currency' => [
                    'type' => 'string'
                ],
            ]
        ];
    }

    public static function getRequestToken()
    {
        return [
            "name" => "RoomerFlex",
            "apiVersion" => "v3",
            "baseUrl" => config('roomerflex_variables.Endpoint'),
            'description' => 'RoomerFlex API for  booking your non-refundable reservation',
            "additionalProperties"=> true,
            'operations' => [
                'requestToken' => self::getRequestTokenOperation()
            ],
            "models"=> [
                "BaseResponse"=> [
                      "type"=> "object",
                      "properties"=> [
                        "success"=> [
                          "type"=> "string",
                          "required"=> true
                        ],
                        "errors"=> [
                          "type"=> "array",
                          "items"=> [
                            "type"=> "object",
                            "properties"=> [
                              "code"=> [
                                "type"=> "string",
                                "description"=> "The error code."
                              ],
                              "message"=> [
                                "type"=> "string",
                                "description"=> "The detailed message from the server."
                              ]
                            ]
                          ]
                        ]
                      ],
                      "additionalProperties"=> [
                        "location"=> "json"
                      ]
                    ],
                'PackageResponse' => self::getRequestTokenResponse()
            ]
        ];
    }

    /*
        End request_token
    */

    /*
    Start create_protection second step
    The protection token generated and sent back in the API response at the first step should be used now in order to make the protection valid and registered.
    */
    static function getProtectionTokenOperation() {
        return [
            "httpMethod" => "POST",
            "uri" => "/api/protection/create_protection",
            "summary" => "Get Package protection token",
            "responseModel" => "ProtectioneResponse",
            "parameters" => [
                "package" => json_decode(file_get_contents(__DIR__.'/Schema/Protection.json'),true)
            ]
        ];
    }

    static function getProtectionTokenResponse() {
        return [
            'type' => 'object',
            "extends"=> "BaseResponse",
            'properties' => [
                'success' => [
                    'type' => 'boolean'
                ],
                'registered' => [
                    'type' => 'boolean'
                ],
                'lh-token' => [
                    'type' => 'string'
                ],
            ]
        ];
    }

    public static function getProtectionToken()
    {
        return [
            "name" => "RoomerFlex",
            "apiVersion" => "v3",
            "baseUrl" => config('roomerflex_variables.Endpoint'),
            'description' => 'RoomerFlex API for  booking your non-refundable reservation',
            "additionalProperties"=> true,
            'operations' => [
                'protectionToken' => self::getProtectionTokenOperation()
            ],
            "models"=> [
                "BaseResponse"=> [
                      "type"=> "object",
                      "properties"=> [
                        "success"=> [
                          "type"=> "string",
                          "required"=> true
                        ],
                        "errors"=> [
                          "type"=> "array",
                          "items"=> [
                            "type"=> "object",
                            "properties"=> [
                              "code"=> [
                                "type"=> "string",
                                "description"=> "The error code."
                              ],
                              "message"=> [
                                "type"=> "string",
                                "description"=> "The detailed message from the server."
                              ]
                            ]
                          ]
                        ]
                      ],
                      "additionalProperties"=> [
                        "location"=> "json"
                      ]
                    ],
                'ProtectioneResponse' => self::getProtectionTokenResponse()
            ]
        ];
    }

    /*
        End create_protection
    */    
}