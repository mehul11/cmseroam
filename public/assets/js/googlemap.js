/**
 * Created by staff777 on 6/13/2016.
 */


$('.go').click(function(e){
    e.preventDefault();
    var country_id = $("#country option:selected").val();
    var city_id = $("#city_id option:selected").val();
    var city_name = $("#city_id option:selected").text();
    var default_route = $("#default_eroam_map option:selected").val();
    //var data_all_cities = [];

    if(country_id == ''){
        $("#country").addClass('with_error');
    }else{
        $("#country").removeClass('with_error');
    }
    if(city_id == ''){
        $("#city_id").addClass('with_error');
    }
    if(default_route == ''){
        $("#default_eroam_map").addClass('with_error');
    }

    data = listofCities(country_id);
    var array =[];
    $.each(data, function(k,val) {

        array.push({
            name :  val.name,
            city_id:val.id,
            lat: val.latlong.lat,
            lng: val.latlong.lng,
        });



    });
    var dummy = {
        data_coord_origin : array[0],
        data_all_cities : array,
        data_init_map : 'preview'
    };

    init();

})




$('#country').change(function(){
    var country_id = $(this).val();
    var container = $('#city_id');
    container.find('option:gt(0)').remove();
    var data = listofCities(country_id);
    $.each(data, function(k,val) {
        container.append('<option value="'+val.id+'">'+val.name+'</option>');
    });



});

$("select").change( function(){
    var  val = $(this).val();

    if(val != null){
        $(this).removeClass('with_error');
    }

    if(val == null || val == ''){
        $(this).addClass('with_error');
    }
});

function listofCities(country_id){
    $.ajax({
        url:'/eroam/api/v1/cities/'+ country_id,
        type:'GET',
        data:{'fn':'GetAllCityData'},
        async:false,
        //cache:false,
        success:function(response){
            if(response.status == 200){
                window.data = response.data;
            }else{
                container.find("option:gt(0)").remove();
            }


        }
    });

    return data;

}

