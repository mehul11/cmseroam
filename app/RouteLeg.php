<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteLeg extends Model {

    protected $table = 'zroutelegs';
    protected $fillable = ['route_id', 'city_id', 'sequence'];
    protected $primaryKey = 'id';

    public function route_plan() {
            return $this->belongsTo('App\Route');
    }
    public function city() 
    {
        return $this->hasOne('App\City','id','city_id')->with('country')->with('latlong');
    }
}