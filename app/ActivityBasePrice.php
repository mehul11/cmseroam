<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityBasePrice extends Model
{
    protected $table = 'zactivitybaseprices';
    protected $primaryKey = 'id';
    protected $fillable = ['activity_price_id','base_price'];
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
