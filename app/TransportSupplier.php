<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportSupplier extends Model {

    protected $table = 'ztransportsuppliers';

    protected $guarded = [];
    
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    use SoftDeletes;

    public static function getTransportSupplierList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return Transport::from('ztransportsuppliers as t')
                        ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                                    $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                                })
                        ->where('t.deleted_at',NULL)
                        ->select(
                            't.*'
                            )
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);  
    }

}
?>