@extends( 'layout/mainlayout' )

@section('content')
    <div class="content-container">
        @if($user_id!="")
        <h1 class="page-title">{{ trans('messages.edit_brand') }}</h1>
        @else
        <h1 class="page-title">{{ trans('messages.add_brand') }}</h1>
        @endif
        <div class="row">
            @if ( Session::has( 'message' ) && Session::get( 'message' ) == 'success' )
                <div class="small-12 small-centered columns success-box">
                    <a href="{{ route('user.create-agent',['nUserId' =>Session::get( 'user_id' ) ]) }}">{{ Session::get( 'brand_name' ) }}</a> 
                    account has been created.
                </div>
            @endif

        </div>	
        <br>
        <form action="{{ route('user.create-brand') }}" method="post" id="addForm">
            {{ csrf_field() }}
            <div class="box-wrapper">
                    <input type="hidden" name="user_id" value="{{ $user_id ??'' }}" />
                <p>{{ trans('messages.brand_detail') }}</p>
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.brand') }}<span class="required">*</span></label>
                    <input type="text" id="name" class="form-control" placeholder="{{ trans('messages.brand_name_placeholder') }}" name="name" value="{{$users->name??''}}"/>
                </div>
                @if ( $errors->first( 'name' ) )
                    <small class="error">{{ $errors->first('name') }}</small>
                @endif
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.produc_email') }} <span class="required">*</span></label>
                    <input type="text" id="email" class="form-control" placeholder="{{ trans('messages.brand_email_placeholder') }}" name="username" value="{{$users->username??''}}" />
                </div>
                @if ( $errors->first( 'username' ) )
                    <small class="error">{{ $errors->first('username') }}</small>
                @endif

                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.password') }} <span class="required">*</span></label>
                    <div class="small-2 column">
                        <a class="prefix toggle-password button secondary tiny btn-sm btn-primary">{{ trans('messages.hide') }}</a> 
                        <input name="password" type="text" value="" id="password">
                        <a href="#" class="button postfix generate-password btn-sm btn-primary">{{ trans('messages.generate') }}</a>
                    </div>
                </div>
                @if ( $errors->first( 'password' ) )
                    <small class="error">{{ $errors->first('password') }}</small>
                @endif                
                <div class="row">
                    <div class="m-t-20 row col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="button success btn btn-primary btn-block" type="submit" value="{{ trans('messages.save_btn') }}">
                            </div>
                            <div class="col-sm-6">
                                <a href="{{ route('user.list',['sUserType' => 'brand' ]) }}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('custom-css')
<style type="text/css">
	.error{
			color:red !important;
	}
	.success-msg {
		background: #67BB67;
		color: #fff;
		padding: 5px;
	}
	.success-msg a {
		color: #fff;
		text-decoration: underline;
	}
	.error_message{
		color:red !important;
	}
	.with_error{
		border-color: red !important;
	}
	.success_message{
		color:green !important;
		text-align: center;
	}
	div .with_error{
		border:1px solid black;
	}
</style>
@stop

@section('custom-js')
<script type="text/javascript">
    $( function() {
        $( '.generate-password' ).click( function( e ) {
            e.preventDefault();
            var generatedHash = Math.random().toString(36).slice(-16).toUpperCase();
            $( 'input[name="password"]' ).val( generatedHash );
        });

        $( '.toggle-password' ).click( function( e ) {
            password = $( 'input[name="password"]' );
            if ( password.attr( 'type' ) == 'text' ) {
                   password.attr( 'type', 'password' );
                   $( this ).text( 'Show' );
            } else {
                   password.attr( 'type', 'text' );
                   $( this ).text( 'Hide' );
            }
        });
    });
    
    $(function() {

        $("#addForm").validate({
            rules: {
                name          : "required",
                domain        : "required",
                username         : "required",
                password      : "required",
                duration      : "required",
            },
            errorPlacement: function(error, element) {
                var placement = $(element).parent();
                if (placement) {
                  $(error).insertAfter(placement)
                } else {
                  error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
     });
</script>
@stop