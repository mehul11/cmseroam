<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\CardInfo;
use Illuminate\Support\Facades\Log;
use Libraries\Yalago;
use App\YalagoCountries;
use App\YalagoProvinces;
use App\YalagoLocations;
use App\YalagoFacilities;

class YalagoApiController extends Controller 
{
	private $host;
	private $apikey;
	private $secret;
	private $cid;
	private $minorRev;
	private $ver;
	private $path;
	private $locale;
	private $currencyCode;
	private $timestamp;
	private $sig;
	private $query;

	public function __construct(){
		parent::__construct();
		$this->api = New Yalago;
		$this->eroamPercentage = 20;
		$this->url 			= 'http://pp.api.yalago.com';
        $this->path         = '/hotels/Inventory/';
		$this->apiKey       = "3c0c1b08-e95e-4d6f-96e4-c8d20bfbb68b";
		//$this->clientName 	= "Rehlaty";
		//$this->itPlatform 	= "E-ROAM";
		$this->ipAddress1 	= '52.41.7.53';
		$this->ipAddress2 	= '52.37.82.218';
		$this->culture 		= 'en-GB';
		$this->currencyCode = 'AUD';
		$this->timestamp 	= gmdate('U');
		$this->customerIpAddress  = $_SERVER['REMOTE_ADDR'];	   
        $this->SourceMarket = 'AE';
	}

    public function getHotels(){
        $data = Input::all();
        //$data = '{"city":"Paris","countryCode":"FR","arrivalDate":"2018-10-30","departureDate":"2018-11-02","numberOfAdults":"1","leg":"0","city_ids":"394","traveller_number":"1","rooms":"1","max_pax":"1","child":"0","search_input":{"_token":"B9xFluU1fs130x4unNoY6yBvi4aigLZCPQAk3tRX","to_country":"575","to_city":"394","auto_populate":"1","option":"manual","end_location":"Paris, France","start_date":"2018-10-21","rooms":"1","travellers":"1","total_children":"0","num_of_pax":["1"],"is_child":{"1":{"1":"1"}},"firstname":{"1":{"1":"rekha"}},"lastname":{"1":{"1":"test"}},"dob":{"1":{"1":"03-05-1985"}},"age":{"1":{"1":"33"}},"num_of_adults":["1"],"num_of_children":["0"],"pax":[[{"firstname":"rekha","lastname":"test","dob":"03-05-1985","age":"33","child":"0"}]],"childrens":"0"},"domain":"localeroam.com"}';
        //$data = json_decode($data,true);
        Log::debug('Yalago Get Hotels');
        Log::debug(json_encode($data));

        $hotels = array();
        $hotels['data'] =  array();

        $city = $data['city'];
        $location = YalagoLocations::getYalagoCity($city)->toArray();
        //echo '<pre>'; print_r($location[0]); die;

        $roomArray = array();
        $rooms = $data['rooms'];
        for ($i=0; $i < $rooms; $i++) { 
            $roomArray[$i]['Adults'] = $data['search_input']['num_of_adults'][$i];

            if(isset($data['search_input']['child'])){
                $roomArray[$i]['ChildAges'] = array_key_exists($i, $data['search_input']['child']) ? $data['search_input']['child'][$i] : array();  
            } else {
                $roomArray[$i]['ChildAges'] = array();
            }
        }

        if(isset($location[0]['yalagoProvinceId'])){
            $path = '/hotels/availability/Get';
            $logname = 'yalago_avaibility';
            $yalagodata = array(
                "CheckInDate"=>$data['arrivalDate'],
                "CheckOutDate"=>$data['departureDate'],
                "ProvinceId"=>$location[0]['yalagoProvinceId'],
                "LocationId"=>$location[0]['yalagoLocationId'],
                "Culture"=>$this->culture,
                "Rooms"=>$roomArray,
                "GetpackagePrice" => true,
                "SourceMarket"=>$this->SourceMarket,
                //"IsFlightPlus" => false,
            );
        
            $hotels = $this->api->getApiData($yalagodata,$path,$logname);
            $hotels = json_decode($hotels->content(),TRUE);
            //echo '<pre>'; print_r(($hotels)); die;

            $hotelAll = array();
            $hotelList = array();
            if(isset($hotels['data']['Establishments'])){
                foreach ($hotels['data']['Establishments'] as $key => $hotel) { 
                    $roomList = array();
                    foreach ($hotel['Rooms'] as $keyroom => $room) {
                        if($room['QuantityAvailable'] > $rooms){
                            $roomList['Rooms'][] = $room;
                            if(empty($hotelList) || !in_array($hotel['EstablishmentId'], $hotelList)){
                                $hotelList[] = $hotel['EstablishmentId'];
                            }
                        }   
                    }

                    if(in_array($hotel['EstablishmentId'], $hotelList)){

                        $path = '/hotels/Inventory/GetEstablishment';
                        $logname = 'yalago_info';
                        $yalagodata = array(
                            "EstablishmentId" =>$hotel['EstablishmentId'],
                            "Languages"=> ['en']  
                        );
                        
                        $hotelDetails = $this->api->getApiData($yalagodata,$path,$logname);
                        $hotelDetails = json_decode($hotelDetails->content(),TRUE);
                        $hotelDetails = $hotelDetails['data']['Establishment'];
                        //echo '<pre>'; print_r(($hotel)); print_r(($hotelDetails));
                        $locationD = YalagoLocations::getYalagoCityName($hotelDetails['LocationId'])->toArray();
                        $hotelAll[] = array_merge($hotelDetails,$locationD[0],$roomList);
                        //echo '<pre>'; print_r($hotelAll); die;
                    } 
                }
            }

            $hotels['data'] = $hotelAll;
        }
        //echo '<pre>'; print_r($hotels); die;
        return \Response::json(['status'=>200,'data'=>$hotels]);
    }

    public function getHotelDetails(){
        $data = Input::all();
        //$data = '{"EstablishmentId":"387576","arrivalDate":"2018-10-30","departureDate":"2018-11-02","rooms":"2","num_of_adults":[2,2],"num_of_children":[0,0],"child":[]}';
        //$data = '{"EstablishmentId":"387576","arrivalDate":"2018-10-30","departureDate":"2018-11-02","rooms":"1","num_of_adults":[2],"num_of_children":[0],"child":[]}';
        //$data = json_decode($data,true);
        Log::debug('Yalago Get Hotels Detail');
        Log::debug(json_encode($data));

        $roomArray = array();
        $rooms = $data['rooms'];
        for ($i=0; $i < $rooms; $i++) { 
            $roomArray[$i]['Adults'] = $data['num_of_adults'][$i];

            if(isset($data['child'])){
                $roomArray[$i]['ChildAges'] = array_key_exists($i, $data['child']) ? $data['child'][$i] : array();  
            } else {
                $roomArray[$i]['ChildAges'] = array();
            } 
        }

        $path = '/hotels/availability/get';
        $logname = 'yalago_avaibility';
        $yalagodata = array(
            "CheckInDate"=>$data['arrivalDate'],
            "CheckOutDate"=>$data['departureDate'],
            "EstablishmentId"=>$data['EstablishmentId'],
            "Culture"=>$this->culture,
            "Rooms"=>$roomArray,
            "GetpackagePrice" => true,
            "SourceMarket"=>$this->SourceMarket,
            //"IsFlightPlus" => false,
        );

        $hotels = $this->api->getApiData($yalagodata,$path,$logname);
        $hotels = json_decode($hotels->content(),TRUE);
        $hotels = $hotels['data'];

        $message['Messages'] = $hotels['Messages'];
        //echo '<pre>'; print_r($hotels); die;

        $roomsDetails = array();

        $InfoItems = array();
        $hotelAllDetails = array();
        $roomPrice = array();
        $allRoomPrice = array();
        if(isset($hotels['Establishments'])){

            $allRooms = array();  $allRooms1 = array();
            foreach ($hotels['Establishments'][0]['Rooms'] as $key => $value) {
                if($value['QuantityAvailable'] > $rooms){
                    //echo $key.'==>'.$value['Description'].'//'.$value['QuantityAvailable'].'<br>';
                    preg_match_all('!\d+!', $value['Description'], $matches);
                    $value['minAdult'] = 1;
                    if(!empty($matches[0])){
                        $maxAdult = $matches[0][1];
                        $value['minAdult'] = trim($matches[0][0]);
                        $value['maxAdult'] = trim($matches[0][1]);
                    } else {
                        $maxAdult = $this->minMaxPass($value['Description']);
                        $value['maxAdult'] = $maxAdult;
                    }

                    $hotels['Establishments'][0]['Rooms'][$key]['minAdult'] = $value['minAdult'];
                    $hotels['Establishments'][0]['Rooms'][$key]['maxAdult'] = $value['maxAdult'] ;

                    $totalPass = 0;
                    for ($i=0; $i < $rooms; $i++) { 
                        $adult = $data['num_of_adults'][$i];
                        $child = 0;
                        if(isset($data['child'])){
                            $child = array_key_exists($i, $data['child']) ? count($data['child'][$i]) : 0;  
                        } 
                        $totalPass = $adult + $child;

                        if($maxAdult == 1){ $maxAdult = $totalPass;}
                        //echo $maxAdult .'>='. $totalPass.'<br>';
                        if($maxAdult >= $totalPass ){
                            foreach ($value['Boards'] as $key1 => $board){
                                $roomPrice[$i][$key.'_'.$key1] = $board['NetCost']['Amount'];
                            }
                        }
                    }
                    //} 

                    $arr = explode("(", $value['Description'], 2);
                    $value['Description']= $arr[0];
                    $hotels['Establishments'][0]['Rooms'][$key]['Description'] = $arr[0];

                    $allRooms['Rooms'][] = $value;
                    foreach ($value['Boards'] as $key1 => $board){
                        if($board['RequestedRoomIndex'] == 0){
                            $allRoomPrice[$key.'_'.$key1] = $board['NetCost']['Amount'];
                        }
                    }

                    //echo '<pre>'; print_r($value);
                    /*foreach ($value['Boards'] as $key1 => $value1) {
                        foreach ($roomArray as $key2 => $value2) {
                            $roomArray[$key2]['RoomCode']  = $value['Code'];
                            $roomArray[$key2]['BoardCode'] = $value1['Code'];
                        }

                        $path2 = '/hotels/details/Get';
                        $logname2 = 'yalago_info';
                        $yalagodata2 = array(
                            "CheckInDate"=>$data['arrivalDate'],
                            "CheckOutDate"=>$data['departureDate'],
                            "EstablishmentId"=>$data['EstablishmentId'],
                            "Culture"=>$this->culture,
                            "Rooms"=>$roomArray,
                            "GetpackagePrice" => true,
                            "SourceMarket"=>$this->SourceMarket,
                            //"IsFlightPlus" => true,
                        );
                        //echo '<pre>'; print_r($yalagodata2); 

                        $hotelGet = $this->api->getApiData($yalagodata2,$path2,$logname2);
                        $hotelGet = json_decode($hotelGet->content(),TRUE);

                        //echo '<pre>'; print_r($hotelGet['data']['Establishment']['Rooms']); 
                        //$string = $hotelGet['data']['Establishment']['Rooms'][0]['Description'];

                        //echo $i.'==>'.$room['Description'].'//'.$room['QuantityAvailable'].'<br>';
                        //preg_match_all('!\d+!', $string, $matches);
                        //print_r($matches);
                        //$arr = explode("(", $string, 2);

                        $InfoItems['InfoItems'] = $hotelGet['data']['InfoItems'][0];
                        $allRooms['Rooms'][] = array_merge($hotelGet['data']['Establishment']['Rooms'][0],$InfoItems);//
                    }*/
                } 
            }
            //echo '<pre>'; print_r($roomPrice); 

            $sortRoomPrice = array();
            if(!empty($allRoomPrice)){
                asort($allRoomPrice);
                //echo '<pre>';  print_r($allRoomPrice); 
                foreach ($allRoomPrice as $allkey => $allvalue ) {
                    $allkey .'=>'. $allvalue.'<br>';
                    $allRoomId = explode('_', $allkey); 

                    $roomBoard = $allRooms['Rooms'][$allRoomId[0]];
                    $roomBoard['Boards'] = $allRooms['Rooms'][$allRoomId[0]]['Boards'][$allRoomId[1]];
                    $sortRoomPrice['Rooms'][] = $roomBoard;  
                }
            }
            //echo '<pre>'; print_r($sortRoomPrice); 

            /*if(!empty($roomPrice)){
                for ($i=0; $i < $rooms; $i++) { 
                    $min_key = array_keys($roomPrice[$i], min($roomPrice[$i]));
                    $min_value = $roomPrice[$i][$min_key[0]];
                    $room_ids = explode('_', $min_key[0]); 

                    $roomArray[$i]['RoomName'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Description'];
                    $roomArray[$i]['BoardName'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Boards'][$room_ids[0]]['Description'];
                    $roomArray[$i]['RoomCode'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Code'];
                    $roomArray[$i]['BoardCode'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Boards'][$room_ids[0]]['Code'];
                    $roomArray[$i]['RoomBoardPrice'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Boards'][$room_ids[0]]['NetCost']['Amount'];
                    $roomArray[$i]['RoomBoardCurrency'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Boards'][$room_ids[0]]['NetCost']['Currency'];
                    $roomArray[$i]['IsBindingPrice'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Boards'][$room_ids[0]]['IsBindingPrice'];

                    if(isset($hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Boards'][$room_ids[0]]['GrossCost'])){
                        $roomArray[$i]['GrossPrice'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Boards'][$room_ids[0]]['GrossCost']['Amount'];
                        $roomArray[$i]['GrossCurrency'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['Boards'][$room_ids[0]]['GrossCost']['Currency'];
                    }
                    $roomArray[$i]['minAdult'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['minAdult'];
                    $roomArray[$i]['maxAdult'] = $hotels['Establishments'][0]['Rooms'][$room_ids[0]]['maxAdult'];
                }
            }

            $RoomSelected['RoomSelected'] = $roomArray;
            //echo '<pre>'; print_r($roomArray); */

            $path1 = '/hotels/Inventory/GetEstablishment';
            $logname1 = 'yalago_info';
            $yalagodata1 = array(
                "EstablishmentId" =>$hotels['Establishments'][0]['EstablishmentId'],
                "Languages"=> ['en'] 
            );

            $hotelDetails = $this->api->getApiData($yalagodata1,$path1,$logname1);
            $hotelDetails = json_decode($hotelDetails->content(),TRUE);
            $hotelDetails = $hotelDetails['data'];

            $FacilitiyArray = array();
            if(!empty($hotelDetails['Establishment']['Facilities'])){
                foreach ($hotelDetails['Establishment']['Facilities'] as $Facilitiy) {
                    $FacilitiyArray[] = $Facilitiy['FacilityId'];
                }
                $facilities = YalagoFacilities::getYalagoFacilityNames($FacilitiyArray)->toArray();
                $hotelDetails['Establishment']['Facilities'] = $facilities;
            }
            
            //echo '<pre>'; print_r($hotelDetails); die;
            $hotelAllDetails = array_merge($hotelDetails['Establishment'],$sortRoomPrice); //,$RoomSelected

        }
        //echo '<pre>'; print_r($hotelAllDetails); 
        return \Response::json(['status'=>200,'data'=>$hotelAllDetails]);
    }

    public function minMaxPass($name)
    {   $max = 1; 
        if (strpos($name, 'Twin') !== false) { $max = 2; }
        elseif (strpos($name, 'Double') !== false) { $max = 2; }
        elseif (strpos($name, 'Privilege') !== false) { $max = 2; }
        elseif (strpos($name, 'Superior') !== false) { $max = 2; }
        elseif (strpos($name, 'Deluxe') !== false) { $max = 3; }
        elseif (strpos($name, 'Luxury') !== false) { $max = 3; }
        elseif (strpos($name, 'Studio') !== false) { $max = 3; }
        elseif (strpos($name, 'Double Double') !== false) { $max = 4; }
        elseif (strpos($name, 'Classic Suite') !== false) { $max = 4; }
        elseif (strpos($name, 'Deluxe Suite') !== false) { $max = 4; }
        return $max;
    }

    public function getHotelsAvailability(){   
        $data = Input::all();
        $path = '/hotels/availability/get';
        /*$data = '{"CheckInDate":"2018-10-23","CheckOutDate":"2018-10-26","EstablishmentId":759174,"Culture":"en-GB","Rooms":[{"Adults":2,"ChildAges":["8"]},{"Adults":1,"ChildAges":["8"]}],"GetpackagePrice":false,"SourceMarket":"AE"}';
        $data = json_decode($data);*/
        $hotels = $this->api->getApiData($data,$path,'yalago_avaibility');
        $hotels = json_decode($hotels->content(),TRUE);        
        return $hotels;
    }

    public function getHotelDetails_book(\Illuminate\Http\Request $request){   
        //$data = Input::all();
        $path = '/hotels/details/Get';
        $data = array(
            "CheckInDate"=>"2018-10-24",
            "CheckOutDate"=>"2018-10-27",
            "EstablishmentId"=>759174,
            "Culture"=>$this->culture,
            "Rooms"=>array(
                array("Adults"=>2,"ChildAges"=>array(8),"RoomCode"=>"CGISBzI5NTU5NzcaBzEzNzU5MDk=", "BoardCode"=>"CgEy"),
                array("Adults"=>1,"ChildAges"=>array(8),"RoomCode"=>"CGISBzI5NTU5NzcaBzEzNzU5MDk=", "BoardCode"=>"CgEy")
                ),
            "GetpackagePrice" => true,
            //"IsFlightPlus" => true,
        );
        
        $hotels = $this->api->getApiData($data,$path,'yalago_info');
        $hotels = json_decode($hotels->content(),TRUE);
        //echo '<pre>'; print_r(($hotels));
        $hotels = $hotels['data'];

        foreach ($hotels as $key => $hotel) {
            foreach ($hotel['Rooms'] as $key1 => $room) {
                foreach ($room['Boards'] as $key2 => $board) {
                    echo '<pre>'; print_r($board['CancellationPolicy']); //die;
                    if(isset($board['CancellationPolicy']) && isset($board['CancellationPolicy']['CancellationCharges'])){
                        
                        $policy = '<span class="widget-tooltip-bd"><ul class="cancellation_penalty_rule_list">';
                        foreach ($board['CancellationPolicy']['CancellationCharges'] as $key2 => $charges) {
                            $policy .= '<li>If you change or cancel your booking on or before '.date('g:i A, d/m/Y', strtotime($charges['ExpiryDate'])).' (GMT) you will be charged for '.$charges['Charge']['Currency'].' '.$charges['Charge']['Amount'].'</li>';
                        }
                        $policy .= '</ul> We will not be able to refund any payment for no-shows or early check-out.</span>';
                        echo $policy;

                    } die;
                }  
            }  
        }


        /*$domain = $request->header('domain');
        if ($domain) {
            $commissions = getDomainData($domain);
            if ($commissions['pricing'][0]['status'] == 0) {
                $hotels = updatePricing($hotels,'hotel','yalago',$commissions['eroam'],$commissions['pricing']);
            }
        }*/
        //return $hotels;
    }

    public function yalagoBooking(){   
        
        $data = Input::all();        
        $path = '/hotels/bookings/create';

        /*$data = array(
            "AffiliateRef" => $random_number1,
            "CheckInDate"=>"2019-07-08",
            "CheckOutDate"=>"2019-07-09",
            "EstablishmentId"=>748868,
            "Culture"=>$this->culture,
            "Rooms"=>array(
                array(
                    "AffiliateRoomRef" => $random_number2, 
                    "RoomCode"=>"CL4fEghKUDg2MDQ1NxoDMzY1", 
                    "BoardCode"=>"CgEyEAE=",
                    "ExpectedNetCost"=> array(
				        "Amount"=> 102.74,
				        "Currency"=> "EUR"
				    ),
                    //"Adults"=>2,
                    //"ChildAges"=>array(),
                    "Guests" => array(
                        array(
                            "Title"=>"Miss",
                            "FirstName"=>"Emily",
                            "LastName"=>"Hanna",
                            "Age" =>30
                        ),
                        /*array(
                            "Title"=>"Mr",
                            "FirstName"=>"Guest",
                            "LastName"=>"Two",
                            "Age" =>30
                        ),*//*
                    ),
                    "SpecialRequests" =>"Non-smoking room please. Ground floor if possible."
                ),
                /*array(
                    "AffiliateRoomRef" => $random_number3, 
                    "RoomCode"=>"CL4fEghKUDg2MDQ1NxoDNDA2", 
                    "BoardCode"=>"CgEyEAE=",
                    //"Adults"=>1,
                    //"ChildAges"=>array(1,7),
                    "Guests" => array(
                        array(
                            "Title"=>"Miss",
                            "FirstName"=>"Rekha",
                            "LastName"=>"Hanna",
                            "Age" =>25
                        ),
                        array(
                            "Title"=>"Mr",
                            "FirstName"=>"Dhara",
                            "LastName"=>"Three",
                            "Age" =>15
                        ),
                        array(
                            "Title"=>"Mr",
                            "FirstName"=>"Priya",
                            "LastName"=>"Four",
                            "Age" =>10
                        ),
                    ),
                    "SpecialRequests" =>"Non-smoking room please. Ground floor if possible."
                )*//*
            ),
            "ContactDetails"=>array(
                "Title"=>"Miss",
                "FirstName"=>"Emily",
                "LastName"=>"HannaUK",
                /*"DateOfBirth"=>"1980-01-01",
                "Address1"=>"adsfsad",
                "Address2"=>"",
                "Address3"=>"",
                "Town"=>"fsadfsadf",
                "County"=>"",
                "PostCode"=>"kt26nh",
                "Country"=>"ES",
                "EmailAddress"=>"test@test.com",
                "HomeTel"=>"9876543210",
                "MobileTel"=>"",
                "WorkTel"=>"",*/ /*
            ),
            /*"PaymentMethod"=>array(
                "CardNumber"=>$creditCardNumber,
                "CardType"=>"Visa",
                "SecurityCode"=>$creditCardIdentifier,
                //"IssueNumber"=>"",
                //"StartMonth"=>"",
                //"StartYear"=>"",
                "EndMonth"=>$creditCardExpirationMonth,
                "EndYear"=>$creditCardExpirationYear, 
            ),*//*
            //"PaymentMethod"=>null,
            //"GetTaxBreakdown"=> false,
            //"GetpackagePrice" => true,
            //"IsFlightPlus" => true,
            //"SourceMarket" =>'',
        );
        //echo '<pre>'; print_r(($data)); die;*/

       /* $data = '{"AffiliateRef":"67387854014","CheckInDate":"2018-10-26","CheckOutDate":"2018-10-29","EstablishmentId":759174,"Culture":"en-GB","Rooms":[{"AffiliateRoomRef":"76580310487","RoomCode":"CGISBzI5NTU5NzcaBzEzNzU5MDk=","BoardCode":"CgEy","ExpectedNetCost":{"Amount":20.66,"Currency":"EUR"},"Guests":[{"Title":"Ms","FirstName":"aone","LastName":"aonesurname","Age":"19"},{"Title":"Mr","FirstName":"atwo","LastName":"atwosurname","Age":"28"},{"Title":"Ms","FirstName":"cone","LastName":"conesurname","Age":"8"}]},{"AffiliateRoomRef":"20043173279","RoomCode":"CGISBzI5NTU5NzcaBzEzNzU5MDk=","BoardCode":"CgEy","ExpectedNetCost":{"Amount":18.47,"Currency":"EUR"},"Guests":[{"Title":"Ms","FirstName":"roomtwoadult","LastName":"adultsurname","Age":"18"},{"Title":"Ms","FirstName":"roomtwochild","LastName":"surname","Age":"17"}]}],"ContactDetails":{"Title":"Ms","FirstName":"aone","LastName":"aonesurname","EmailAddress":"dhara@loftgroup.com.au"}}';
        $data = json_decode($data);*/
       
        $booking = $this->api->getApiData($data,$path,'yalago_booking');
        $response = json_decode($booking->content(),TRUE);
        return $response;
    }

    public function GetCountries() {
        /*$method = 'GetCountries'; 
       /*$method = 'GetProvinces';
        $method = 'GetLocations';
        $method = 'GetEstablishments';
        $method = 'GetEstablishment';
        //$method = 'GetEstablishmentDiff';*/
        $method = 'GetFacilities';
        
        //$path1 = $this->path.'GetFacilities';
        $data = array(
            //"CountryId"  => 3557,
            //"ProvinceId" => 74304,
            //"LocationId" => 1019197,
            //"EstablishmentId" => 9007083,
            //"UpdatesAfter" => "2018-02-22T10:26:13Z",
            "Languages"  => ["en","es"]
        );
       //echo '<pre>'; print_r($data);
        $query = json_encode($data);


        //$this->path = '/hotels/availability/Get';
        //$method = ''; 

        /*$data = array(
            "CheckInDate"=>"2019-07-08",
            "CheckOutDate"=>"2019-07-09",
            //"ProvinceId"=>54634,
            //"LocationId"=>0,
            //"EstablishmentIds"=>array(111767,748868),
            "EstablishmentId"=>111767,
            "Culture"=>$this->culture,
            "Rooms"=>array(
                array("Adults"=>2,"ChildAges"=>array()),
                array("Adults"=>1,"ChildAges"=>array(1,7))
                ),
            "GetpackagePrice" => true,
            "SourceMarket"=>$this->SourceMarket,
            //"IsFlightPlus" => true,
        );
        $query = json_encode($data);*/

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . $this->path."{$method}");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$query);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('x-api-key:'.$this->apiKey,'Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //getting response from server
        $data = curl_exec($ch);
        $headers = curl_getinfo($ch);
        curl_close($ch);
        echo '<pre>'; print_r($headers); print_r(json_decode($data));   

        $facilities = json_decode($data,true);
        foreach ($facilities['Facilities'] as $key => $result) {
            $data_store = [
                'yalagoFacilityId' => $result['FacilityId'],                        
                'Title' => $result['Title']['en'],
                'FacilityType' => $result['FacilityType'],
                'FacilityGroup' => $result['FacilityGroup']
            ];
            YalagoFacilities::firstOrCreate($data_store);
            echo '<pre>'; print_r($data_store);
        }

        die;
        $country = json_decode($data,true);
        
        Log::channel('yalago_static')->info('Response : '.($country));
        Log::channel('yalago_static')->info('--------------------------------------------');
    }

    public function StoreFacilities() {
        $data = array("Languages"  => ["en"]);
        $method = 'GetFacilities'; 
        $path = $this->path.$method;
        $results = $this->api->getApiData($data,$path,'yalago_static');
        $results = json_decode($results->content(),TRUE);
        //echo '<pre>'; print_r($results['data']); die;

        foreach ($results['data']['Facilities'] as $result) {
            $data_store = [
                'yalagoFacilityId' => $result['FacilityId'],                        
                'Title' => $result['Title']['en'],
                'FacilityType' => $result['FacilityType'],
                'FacilityGroup' => $result['FacilityGroup']
            ];
            YalagoFacilities::firstOrCreate($data_store);
            
        } 
    }

    public function StoreCountries()
    {
        $method = 'GetCountries'; 
        $path = $this->path.$method;
        $results = $this->api->getApiData([],$path,'yalago_static');
        $results = json_decode($results->content(),TRUE);
        foreach ($results['data']['Countries'] as $result) 
        {
            $data = [
                    'yalagoCountryId' => $result['CountryId'],
                    'countryCode' => $result['CountryCode'],
                    'countryName' => $result['Title']
                ];
            YalagoCountries::firstOrCreate($data);
        }
    }

    public function StoreProvince()
    {
        $method = 'GetProvinces'; 
        $path = $this->path.$method;

        $yalagoCountries = YalagoCountries::get();
        foreach ($yalagoCountries as $country) {
            $results = [];
            $data = array(
                "CountryId"  => $country['yalagoCountryId'],
            );
            $results = $this->api->getApiData($data,$path,'yalago_static');
            $results = json_decode($results->content(),TRUE);

            foreach ($results['data']['Provinces'] as $result) 
            {
                $data_store = [
                        'yalagoProvinceId' => $result['ProvinceId'],
                        'countryId' => $country['countryID'],
                        'yalagoCountryId' => $country['yalagoCountryId'],
                        'provinceName' => $result['Title']                        
                    ];
                YalagoProvinces::firstOrCreate($data_store);
            }
        }        
    }

    public function StoreLocations()
    {
        $method = 'GetLocations'; 
        $path = $this->path.$method;

        $yalagoProvinces = YalagoProvinces::get();
        foreach ($yalagoProvinces as $province) {
            $results = [];
            $data = array(
                "CountryId"  => $province['yalagoCountryId'],
                "ProvinceId" => $province['yalagoProvinceId'],
            );
            $results = $this->api->getApiData($data,$path,'yalago_static');
            $results = json_decode($results->content(),TRUE);
            if(isset($results['data']['Locations']))
            {
                foreach ($results['data']['Locations'] as $result) 
                {
                    $data_store = [
                            'yalagoLocationId' => $result['LocationId'],                        
                            'yalagoProvinceId' => $province['yalagoProvinceId'],
                            'yalagoCountryId' => $province['yalagoCountryId'],
                            'countryId' => $province['countryId'],                        
                            'provinceId' => $province['provinceId'],
                            'locationName' => $result['Title']                        
                        ];
                    YalagoLocations::firstOrCreate($data_store);
                }
            }
        }        
    }

}