<?php //echo "<pre>";print_r($oSuburbList);exit; ?>
@foreach ($oSuburbList as $aCountry) 
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aCountry->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aCountry->id;?>" value="<?php echo $aCountry->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="#">
                {{ $aCountry->name }}
            </a>
        </td>
        <td>{{ $aCountry->city_name }}</td>
        <td>{{ $aCountry->country_name }}</td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <a href="{{ route('common.create-suburb',['nIdSuburb'=>$aCountry->id])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn')}}</a>
            </div>
        </td>
    </tr> 
@endforeach