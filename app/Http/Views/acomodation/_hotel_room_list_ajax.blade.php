<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getSortData(this,'name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getSortData(this,'pax');">{{ trans('messages.pax') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'pax')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th class="text-center">{{ trans('messages.action_head') }} </th>
        </tr>
    </thead>
    <tbody class="hotel_list_ajax">
    @if(count($oHotelList) > 0)
        @foreach ($oHotelList as $aHotel)	
            <tr>
                <td>
                    <label class="radio-checkbox label_check" for="checkbox-<?php echo $aHotel->id;?>">
                        <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aHotel->id;?>" value="<?php echo $aHotel->id;?>">&nbsp;
                    </label>
                </td>
                <td>
                    <a href="#">
                        {{ $aHotel->name }}
                    </a>
                </td>
                <td>
                    <a href="#">
                        {{ $aHotel->pax }}
                    </a>
                </td>
                <td class="text-center">
                    <div class="switch tiny switch_cls">
                        <a href="{{ route('acomodation.hotel-roomtype-create',[ 'nIdLabel' => $aHotel->id  ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                        <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('acomodation.room-delete',['nIdLabel'=> $aHotel->id]) }}','{{ trans('messages.delete_label')}}')">
                    </div>
                </td>
            </tr> 
        @endforeach
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',[0 , 'total'=>$oHotelList->count() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>