<?php
namespace Libraries;
use GuzzleHttp\Client;

class Gadventure
{
	public function __construct(){
        $this->url  	= config()->get('services.gadventure.gadventure_url');
	 	$this->api_key  = config()->get('services.gadventure.api_key');
    }

    public function get_data($path,$id = null)
    {   	
    	if(isset($id))
    	{
    		$path = $path."/".$id;
    	}
    	return $this->process_request(null,'get',$path,null);
    }



    public function process_request($body,$method,$path,$string){
    	$client 	= new Client();   
    	$response 	= [];     
    	$headers	= 	[
							'X-Application-Key' => $this->api_key,
							'Origin' => url('')
				  		];
		
		try 
		{			
			$response = json_decode( $client->$method( $this->url . $path, [
				'body' 		=> $body,
				'headers' 	=> $headers
			] )->getBody(), true );				
			$ctr = 20;			
			
		} 
		catch(Exception $e) 
		{
			$ctr++;
			Log::error('An error occured in maps function name(): ' . $e->getMessage());
			sleep(1);
		}
    	
        return  $response;   
    }
}
?>